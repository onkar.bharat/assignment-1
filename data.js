window.addEventListener("load", (e) => {
  const email = sessionStorage.getItem("email");
  const password = sessionStorage.getItem("password");
  const gender = sessionStorage.getItem("gender");
  const role = sessionStorage.getItem("role");
  const permissions = sessionStorage.getItem("permissions");

  document.getElementById("email").innerHTML = email;
  document.getElementById("password").innerHTML = password;
  document.getElementById("gender").innerHTML = gender;
  document.getElementById("role").innerHTML = role;
  document.getElementById("permissions").innerHTML = permissions;
});
