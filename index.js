const name = document.getElementById("name");
const email = document.getElementById("email");
const mobileNu = document.getElementById("mobileNu");
const password = document.getElementById("password");
const role = document.getElementsByName("Role ");
const permission = document.getElementsByName("permission");
const gender = document.getElementById("sex");

let success = document.getElementById("success");
success.style.display = "none";
let danger = document.getElementById("danger")
danger.style.display = "none";

validEmail = false;
validPhone = false;
ValidName = false;
ValidPassword = false;

name.addEventListener('blur', () => {
    let nam = /^[a-zA-Z]([a-zA-Z0-9]){1,10}$/;
    let str = name.value;
    if (nam.test(str)) {
        console.log('name is corrected and is valid');
        name.classList.remove('is-invalid');
        ValidName = true;
        sessionStorage.setItem("name", str);
    } else {
        console.log('not valid');
        ValidName = false;
        name.classList.add('is-invalid');
    }

})

email.addEventListener('blur', () => {
    let eml = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    ;
    let str = email.value;
    if (eml.test(str)) {
        console.log('email is corrected and is valid');
        email.classList.remove('is-invalid');
        validEmail = true;
        localStorage.setItem("Email", str)
        sessionStorage.setItem("email", str);
    } else {
        console.log('not valid');
        validEmail = false;
        email.classList.add('is-invalid');
    }

})

password.addEventListener('blur', () => {
    let mobil = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
    let str = password.value;
    if (mobil.test(str)) {
        console.log('password is corrected and is valid');
        password.classList.remove('is-invalid');
        ValidPassword = true;
        sessionStorage.setItem("password", str);

    } else {
        console.log('not valid');
        ValidPassword = false;
        password.classList.add('is-invalid');
    }

})

gender.addEventListener('blur', () => {
    if (gender.value == "null") {
        alert('select your gender first')
    }
    sessionStorage.setItem("gender", gender);
})
const checkRole= () => {
    let isRoleChecked = false;
    let role = null;
    for (let i = 0; i < roles.length; i++) {
        if (roles[i].checked) {
            isRoleChecked = true;
            role = roles[i].value;
            break;
        }
    }
    if (!isRoleChecked) {
        alert("Please select a role");
        return false;
    }
    sessionStorage.setItem("role", role);
    return true;
}
const Checkpermission= () => {
    var checkBoxes = document.getElementsByClassName('myCheckBox');
    // console.log(checkBoxes)
    var count = 0;
    let perm = [];
    for (var i = 0; i < checkBoxes.length; i++) {
        const elem = checkBoxes[i];
        if (elem.checked) {
            count++;
            perm.push(elem.value);
            isChecked = true;
        };
    };
    // console.log(count)
    if (count < 2) {
        return alert('At least one checkbox checked!');

    }
    sessionStorage.setItem("permissions", perm.join(","));
    return true;
}

let submit = document.getElementById("submit");
submit.addEventListener("click", (a) => {
    let success = document.getElementById("success");
    let danger = document.getElementById("danger")
    a.preventDefault();

    if (ValidName && validEmail && ValidPassword &&checkRole && Checkpermission) {
        success.style.display = "block";
        danger.style.display = "none";
        window.open("data.html", "_self");

    } else {
        danger.style.display = "block";
        success.style.display = "none";

    }

})